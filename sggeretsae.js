      window.onload = function() {
          ListenPresses()
      };
      var running = [];
      var konamiCode = '38,38,40,40,37,39,37,39,66,65'; //UP,UP,Down,Down,Left,Right,Left,Right,B,A
      var awesomeCode = '65,87,69,83,79,77,69'//AWESOME
      var rickrollCode = '82,73,67,75,82,79,76,76'; //RICKROLL
      var ricardoCode = '82,73,67,65,82,68,79'; //RICARDO
      var keyPresses = [];
      var ListenPresses = function() {
          document.addEventListener('keydown', function(event) {
              if (keyPresses.length > 15){
                  keyPresses.splice(0,1);
              }
              keyPresses.push(event.keyCode);
              if (keyPresses.join().includes(konamiCode)){
                  keyPresses.splice(0,15)
                  KonamiScript();
                  //debugline: document.getElementById("someclass").innerHTML="Konami Code Activated"; 
              }
              if (keyPresses.join().includes(awesomeCode)){
                  keyPresses.splice(0,15)
                  if (!running.includes("Rainbow")){
                    RainbowScript();
                    running.push("Rainbow");
                  }
                  //debugline: document.getElementById("someclass").innerHTML="Awesome Code Activated";
              }
              if (keyPresses.join().includes(rickrollCode)){
                  keyPresses.splice(0,15)
                  if (!running.includes("RickRoll")){
                    RickrollScript();
                    running.push("RickRoll");
                  }
                  //debugline: document.getElementById("someclass").innerHTML="Awesome Code Activated";
              }
              if (keyPresses.join().includes(ricardoCode)){
                  keyPresses.splice(0,15)
                  if (!running.includes("Ricardo")){
                    RicardoScript();
                    running.push("Ricardo");
                  }
                  //debugline: document.getElementById("someclass").innerHTML="Awesome Code Activated";
              }
              //debugline: document.getElementByClassName("container").innerHTML=keyPresses;
          });
      };
  
      var KonamiScript = function() {
          new Audio('http://s3.amazonaws.com/moovweb-marketing/playground/harlem-shake.mp3').play();
          setTimeout(function() {
              var x = document.querySelectorAll("div");
              x.forEach(element => {
                  HarlemShake(element);
              });
              setTimeout(function() {
              //debugline: document.getElementById("someclass").innerHTML="Animation Stopped"; 
              
              }, 14000);
          }, 15550);
          var HarlemShake = function (element) {
              var maxmove = 50;
              var counter = 1;
              var numberOfShakes = 800;
              var startX = 0, startY = 0, startAngle = 0;
              var moveUnit = maxmove / numberOfShakes;
              var randomInt = (min, max) => {
                  return Math.floor(Math.random() * (max - min + 1)) + min;
              };
              upAndDownShake();
              function upAndDownShake() {
                  if (counter < numberOfShakes) {
                      element.style.transform = 'translate(' + startX + 'px, ' + startY + 'px)';
                      maxmove -= moveUnit;
                      var randomX = randomInt(-maxmove, maxmove);
                      var randomY = randomInt(-maxmove, maxmove);
                      element.style.transform = 'translate(' + randomX + 'px, ' + randomY + 'px)';
                      counter += 1;
                      requestAnimationFrame(upAndDownShake);
                  }
                  if (counter >= numberOfShakes) {
                  element.style.transform = 'translate(' + startX + ', ' + startY + ')';
                  }
              }
          };
      };
  
      var RainbowScript = function() {
          var x = document.querySelectorAll("div");
          x.forEach(element => {
              var r=0,g=0,b=0,black=255;
              var refreshIntervalId = setInterval(function(){
                      if(r > 0 && black == 0){
                          r--;
                          g++;
                      }
                      if(g > 0 && r == 0){
                          g--;
                          b++;
                      }
                      if(b > 0 && g == 0){
                          b--;
                          black++;
                      }
                      if(black > 0 && b == 0){
                          black--;
                          r++;
                      }
                      element.style.color = "rgb("+r+","+g+","+b+")";
                  },10);
              element.style.color = "black";
          });
      };
  
      var RickrollScript = function() {
          new Audio('https://www.myinstants.com/media/sounds/never-gonna-give-u-up.mp3').play();
          setInterval(function() {
              new Audio('https://www.myinstants.com/media/sounds/never-gonna-give-u-up.mp3').play();
              }, 8200);
  
          var x = document.querySelectorAll("img");
              x.forEach(element => {
                  element.src = "https://media0.giphy.com/media/5kq0GCjHA8Rwc/giphy.gif";
              });
          var y = document.querySelectorAll("div");
              y.forEach(element => {
                if (element.style.backgroundImage){
                  element.style.backgroundImage = "url('https://media0.giphy.com/media/5kq0GCjHA8Rwc/giphy.gif')";
                }
              });
      };
  
      var RicardoScript = function() {
        function noScroll() {
          window.scrollTo(0, 0);
        }
        function fadeOutEffect() {
          var fadeTarget = document.getElementById("ricardoimg");
          console.warn("Fading started")
          var fadeEffect = setInterval(function () {
              if (!fadeTarget.style.opacity) {
                  fadeTarget.style.opacity = 1;
              }
              if (fadeTarget.style.opacity > 0) {
                  fadeTarget.style.opacity -= 0.1;
              } else {
                  clearInterval(fadeEffect);
              }
          }, 200);
        }
        var random = Math.floor(Math.random() * 2); 
        var node = document.createElement("img"); 
        var time;
          if (random == 1){
            var audio = new Audio('https://www.myinstants.com/media/sounds/danc.mp3').play();
            node.src = "https://media1.tenor.com/images/a0728a1f37a63db3e2d0d3b7a0dfcc20/tenor.gif";
            time = 15000;
          } else {
            var audio = new Audio('https://www.myinstants.com/media/sounds/u-got-that-full-version-mmv-audiotrimmer_SS9p2jk.mp3').play();
            node.src = "https://i.kym-cdn.com/photos/images/original/001/479/169/c57.gif";
            time = 11000;
          }
        //dynamic styles change
        node.style.position = "absolute"; node.style.margin = "auto"; node.style.position = "absolute"; node.style.top = "0px"; node.style.left = "0px"; node.style.bottom = "0px"; node.style.right = "0px"; node.width = window.innerWidth; node.height = window.innerHeight; node.style.zIndex = "100000"; node.id = "ricardoimg";
        window.addEventListener('scroll', noScroll);
        var x = document.querySelectorAll("body");
            x.forEach(element => {
             element.appendChild(node);
             setTimeout(function() {
               fadeOutEffect();
             }, time-2000);
             setTimeout(function() {
              element.removeChild(node);
              window.removeEventListener('scroll', noScroll);
              running.splice( running.indexOf('Ricardo'), 1 );
              }, time);
            });
      };
  
      //https://media1.tenor.com/images/a0728a1f37a63db3e2d0d3b7a0dfcc20/tenor.gif?itemid=13894212